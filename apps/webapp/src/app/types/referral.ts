export interface Referral {
  id: number;
  givenName: string;
  surName: string;
  email: string;
  phone: string;
}

export interface NewReferralEntry {
  givenName: string;
  surName: string;
  email: string;
  phone: string;
}
