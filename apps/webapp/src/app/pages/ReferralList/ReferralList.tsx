import React, { useEffect, useState } from 'react';
import { ReferralTable } from '../../components/ReferralTable';
import { Referral } from '../../types/referral';
import Button from '@material-ui/core/Button';
import style from './ReferralList.module.css';
import { CreateOrEditReferralModal } from '../../components/CreateOrEditReferralModal/CreateOrEditReferralModal';

const ReferralList: React.FC = () => {
  const [referrals, setReferrals] = useState<Referral[]>([]);
  const [createReferralModalOpen, setCreateReferralModalOpen] = useState(false);

  useEffect(() => {
    onFetchAll();
  }, []);

  const onFetchAll = async () => {
    try {
      const response = await fetch('http://localhost:3333/referrals');
      if (!response.ok) {
        throw Error(response.statusText);
      }
      const referrals = await response.json();
      setReferrals(referrals);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={style.frame}>
      <ReferralTable referrals={referrals} onFetchAll={onFetchAll} />
      <div>
        <Button variant="contained" color="primary" onClick={() => setCreateReferralModalOpen(true)}>
          Create new
        </Button>
      </div>

      <CreateOrEditReferralModal
        open={createReferralModalOpen}
        handleClose={() => setCreateReferralModalOpen(false)}
        isCreating={true}
        referral={null}
        onFetchAll={onFetchAll}
      />
    </div>
  );
};

export { ReferralList };
