import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { Referral } from '../../types/referral';
import { DeleteModal } from './DeleteModal';

describe('Modal for create or edit referral', () => {
  const dummyReferral: Referral = {
    id: 1,
    givenName: 'John',
    surName: 'Doe',
    phone: '0456 123123',
    email: 'testing@brighte.com.au',
  };

  it('should render delete modal', async () => {
    const { getByTestId } = render(
      <DeleteModal open={true} referral={null} onFetchAll={() => {}} handleClose={() => {}} />
    );
    await waitFor(() => screen.getByRole('presentation'));

    expect(getByTestId('delete-button').textContent).toEqual('Delete');
    expect(getByTestId('cancel-button').textContent).toEqual('Cancel');
  });
});
