import React from 'react';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { Referral } from '../../types/referral';

interface DeleteModalProps {
  open: boolean;
  handleClose: () => void;
  onFetchAll: () => void;
  referral: Referral;
}

const DeleteModal: React.FC<DeleteModalProps> = ({ open, handleClose, onFetchAll, referral }) => {
  const handleDelete = async () => {
    try {
      const response = await fetch(`http://localhost:3333/referrals/${referral.id}`, {
        method: 'DELETE',
      });
      handleClose();
      if (!response.ok) {
        throw Error(response.statusText);
      }
      onFetchAll();
    } catch (error) {
      console.log(error);
    }
  };

  const body = (
    <div>
      <h2 id="modal-title">Are you sure you want to delete this record?</h2>
      <Button variant="contained" color="secondary" onClick={handleDelete} data-testid="delete-button">
        Delete
      </Button>
      <Button variant="contained" onClick={handleClose} data-testid="cancel-button">
        Cancel
      </Button>
    </div>
  );

  return (
    <div>
      <Modal open={open} onClose={handleClose} aria-labelledby="modal-title" aria-describedby="modal-description">
        {body}
      </Modal>
    </div>
  );
};

export { DeleteModal };
