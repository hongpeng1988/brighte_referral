import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React from 'react';
import { ReactComponent as CreateIcon } from '../../../assets/create-24px.svg';
import { ReactComponent as DeleteIcon } from '../../../assets/delete-24px.svg';
import { Referral } from '../../types/referral';
import { CreateOrEditReferralModal } from '../CreateOrEditReferralModal/CreateOrEditReferralModal';
import { DeleteModal } from '../DeleteModal';
import { IconButton } from '../IconButton';
import style from './ReferralTable.module.css';

const TableHeadCell: React.FC = ({ children }) => (
  <TableCell classes={{ root: style.tableHeadCell }}>{children}</TableCell>
);

const TableBodyCell: React.FC = ({ children }) => (
  <TableCell classes={{ root: style.tableBodyCell }}>{children}</TableCell>
);

interface ActionBodyCellProps {
  onEditClick: () => void;
  onDeleteClick: () => void;
}

const ActionBodyCell: React.FC<ActionBodyCellProps> = ({ onEditClick, onDeleteClick }) => (
  <TableCell classes={{ root: style.actionBodyCell }}>
    <IconButton onClick={onEditClick}>
      <CreateIcon />
    </IconButton>
    <IconButton onClick={onDeleteClick}>
      <DeleteIcon />
    </IconButton>
  </TableCell>
);

interface ReferralTableProps {
  referrals: Referral[];
  onFetchAll: () => void;
}

const ReferralTable: React.FC<ReferralTableProps> = ({ referrals, onFetchAll }) => {
  const [deleteModalOpen, setDeleteModalOpen] = React.useState(false);
  const [editModalOpen, setEditModalOpen] = React.useState(false);
  const [selectedReferral, setSelectedReferral] = React.useState<Referral>();

  return (
    <>
      <DeleteModal
        open={deleteModalOpen}
        handleClose={() => {
          setDeleteModalOpen(false);
          setSelectedReferral(null);
        }}
        onFetchAll={onFetchAll}
        referral={selectedReferral}
      />

      <CreateOrEditReferralModal
        open={editModalOpen}
        handleClose={() => {
          setEditModalOpen(false);
          setSelectedReferral(null);
        }}
        referral={selectedReferral}
        onFetchAll={onFetchAll}
        isCreating={false}
      />

      <TableContainer classes={{ root: style.container }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell>Given Name</TableHeadCell>
              <TableHeadCell>Surname</TableHeadCell>
              <TableHeadCell>Email</TableHeadCell>
              <TableHeadCell>Phone</TableHeadCell>
              <TableHeadCell>Actions</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {referrals.map((referral) => (
              <TableRow key={referral.id}>
                <TableBodyCell>{referral.givenName}</TableBodyCell>
                <TableBodyCell>{referral.surName}</TableBodyCell>
                <TableBodyCell>{referral.email}</TableBodyCell>
                <TableBodyCell>{referral.phone}</TableBodyCell>
                <ActionBodyCell
                  onEditClick={() => {
                    setSelectedReferral(referral);
                    setEditModalOpen(true);
                  }}
                  onDeleteClick={() => {
                    setSelectedReferral(referral);
                    setDeleteModalOpen(true);
                  }}
                />
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export { ReferralTable };
