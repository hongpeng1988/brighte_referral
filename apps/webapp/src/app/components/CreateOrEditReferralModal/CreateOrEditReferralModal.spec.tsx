import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { Referral } from '../../types/referral';
import { CreateOrEditReferralModal } from './CreateOrEditReferralModal';

describe('Modal for create or edit referral', () => {
  const dummyReferral: Referral = {
    id: 1,
    givenName: 'John',
    surName: 'Doe',
    phone: '0456 123123',
    email: 'testing@brighte.com.au',
  };

  it('should render create modal', async () => {
    const { getByTestId } = render(
      <CreateOrEditReferralModal
        open={true}
        referral={null}
        isCreating={true}
        onFetchAll={() => {}}
        handleClose={() => {}}
      />
    );
    await waitFor(() => screen.getByRole('presentation'));

    expect(getByTestId('givenName').getAttribute('value')).toEqual('');
    expect(getByTestId('surname').getAttribute('value')).toEqual('');
    expect(getByTestId('email').getAttribute('value')).toEqual('');
    expect(getByTestId('phone').getAttribute('value')).toEqual('');
  });

  it('should render edit modal', async () => {
    const { getByTestId } = render(
      <CreateOrEditReferralModal
        open={true}
        referral={dummyReferral}
        isCreating={false}
        onFetchAll={() => {}}
        handleClose={() => {}}
      />
    );

    expect(getByTestId('givenName').getAttribute('value')).toEqual('John');
    expect(getByTestId('surname').getAttribute('value')).toEqual('Doe');
    expect(getByTestId('phone').getAttribute('value')).toEqual('0456 123123');
    expect(getByTestId('email').getAttribute('value')).toEqual('testing@brighte.com.au');
  });
});
