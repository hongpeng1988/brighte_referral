import React, { useState, useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { Referral, NewReferralEntry } from '../../types/referral';

interface CreateOrEditReferralModalProps {
  open: boolean;
  referral: Referral | null;
  isCreating: boolean;
  handleClose: () => void;
  onFetchAll: () => void;
}

const CreateOrEditReferralModal: React.FC<CreateOrEditReferralModalProps> = ({
  open,
  isCreating,
  referral,
  handleClose,
  onFetchAll,
}) => {
  const [giveName, setGivenName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');

  useEffect(() => {
    setGivenName(referral?.givenName);
    setSurname(referral?.surName);
    setEmail(referral?.email);
    setPhone(referral?.phone);
  }, [referral]);


  const handleCreateOrEdit = () => {
    const newReferralEntry = {
      givenName: giveName,
      surName: surname,
      email: email,
      phone: phone,
    };

    if (isCreating) {
      onCreate(newReferralEntry);
    } else {
      onEdit(referral, newReferralEntry);
    }
  };

  const onEdit = async (referral: Referral, newReferralEntry: NewReferralEntry) => {
    try {
      const response = await fetch(`http://localhost:3333/referrals/${referral.id}`, {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newReferralEntry),
      });
      handleClose();
      if (!response.ok) {
        throw Error(response.statusText);
      }
      onFetchAll();
    } catch (error) {
      console.log(error);
    }
  };

  const onCreate = async (newReferralEntry: NewReferralEntry) => {
    try {
      const response = await fetch('http://localhost:3333/referrals/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newReferralEntry),
      });
      if (!response.ok) {
        throw Error(response.statusText);
      }
      handleClose();
      onFetchAll();
    } catch (error) {
      console.log(error);
    }
  };

  const body = (
    <div>
      <h2 id="modal-title">{isCreating? "Create referral" : "Edit referral"}</h2>
      <div id="modal-body">
        <label>Given Name:</label>
        <input type="text" value={giveName || ''} onChange={(e) => setGivenName(e.target.value)} data-testid="givenName" />
        <label>Surname:</label>
        <input type="text" value={surname || ''} onChange={(e) => setSurname(e.target.value)} data-testid="surname" />
        <label>Email:</label>
        <input type="email" value={email || '' } onChange={(e) => setEmail(e.target.value)} data-testid="email" />
        <label>Phone:</label>
        <input type="text" value={phone || ''} onChange={(e) => setPhone(e.target.value)} data-testid="phone" />
      </div>
      <div>
        <Button variant="contained" color="secondary" onClick={handleClose} data-testid='cancel-button'>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={handleCreateOrEdit} data-testid='save-button'>
          {isCreating ? 'Create' : 'Save'}
        </Button>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-title"
        aria-describedby="modal-body"
      >
        {body}
      </Modal>
    </div>
  );
};

export { CreateOrEditReferralModal };
