import * as cors from 'cors';
import * as express from 'express';
import ReferralController from './referrals/controllers/ReferralController';

const app = express();

app.use(cors());
app.use(express.json());

const referralController = new ReferralController();

app.get('/referrals', referralController.getAllReferrals.bind(referralController));
app.get('/referrals/:id', referralController.getReferralById.bind(referralController));
app.patch('/referrals/:id', referralController.updateById.bind(referralController));
app.delete('/referrals/:id', referralController.deleteById.bind(referralController));
app.post('/referrals/', referralController.createReferral.bind(referralController));

export default app;
