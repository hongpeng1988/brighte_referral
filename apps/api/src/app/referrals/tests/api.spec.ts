import * as request from 'supertest';
import app from '../../';
import prisma from '../../prisma';

describe('Referrals API', () => {
  let dummyReferralRecord1;
  let dummyReferralRecord2;
  let nonExistingId = 999;

  beforeEach(async function () {
    await prisma.referral.deleteMany({});

    dummyReferralRecord1 = await prisma.referral.create({
      data: {
        givenName: 'dummyGivenName1',
        surName: 'dummySurname1',
        email: 'dummy1@email.com',
        phone: '0425555555',
      },
    });

    dummyReferralRecord2 = await prisma.referral.create({
      data: {
        givenName: 'dummyGivenName2',
        surName: 'dummySurname2',
        email: 'dummy2@email.com',
        phone: '0425555555',
      },
    });
  });

  afterEach(async function () {
    await prisma.referral.deleteMany({});
  });

  it('should get all referral records', async () => {
    const result = await request(app).get('/referrals');
    expect(result.status).toEqual(200);
    expect(result.body.length).toBe(2);
  });

  it('should return one specific referral record', async () => {
    const result = await request(app).get(`/referrals/${dummyReferralRecord1.id}`);

    expect(result.status).toEqual(200);
    expect(result.body.givenName).toBe('dummyGivenName1');
    expect(result.body.surName).toBe('dummySurname1');
    expect(result.body.email).toBe('dummy1@email.com');
    expect(result.body.phone).toBe('0425555555');
  });

  it('should return record not found', async () => {
    const result = await request(app).get(`/referrals/${nonExistingId}`);

    expect(result.status).toEqual(404);
    expect(result.body.errors[0]).toBe('Record not found');
  });

  it('should update successfully', async () => {
    const result = await request(app).patch(`/referrals/${dummyReferralRecord1.id}`).send({
      givenName: 'newDummyGivenName',
      surName: 'newDummySurname',
      email: 'new@email.com',
      phone: '0425555556',
    });

    expect(result.status).toEqual(200);
    expect(result.body.givenName).toBe('newDummyGivenName');
    expect(result.body.surName).toBe('newDummySurname');
    expect(result.body.email).toBe('new@email.com');
    expect(result.body.phone).toBe('0425555556');
  });

  it('should return record not found when update with non existing id', async () => {
    const result = await request(app).patch(`/referrals/${nonExistingId}`).send({
      givenName: 'newDummyGivenName',
      surName: 'newDummySurname',
      email: 'new@email.com',
      phone: '0425555556',
    });

    expect(result.status).toEqual(404);
    expect(result.body.errors[0]).toBe('Record not found');
  });

  it('should update successful when partial update', async () => {
    const result = await request(app).patch(`/referrals/${dummyReferralRecord1.id}`).send({
      givenName: 'newDummyGivenName',
    });

    expect(result.status).toEqual(200);
    expect(result.body.givenName).toBe('newDummyGivenName');
  });

  it('should return errors when data is invalid', async () => {
    const result = await request(app).patch(`/referrals/${dummyReferralRecord1.id}`).send({
      givenName: 'a',
      surName: 'b',
      email: 'invalidEmail',
      phone: '123456789',
    });

    expect(result.status).toEqual(400);
    expect(result.body.errors.length).toBe(4);

    expect(result.body.errors[0]).toBe('Given Name should be 2 to 200 characters long');
    expect(result.body.errors[1]).toBe('Surname should be 2 to 200 characters long');
    expect(result.body.errors[2]).toBe('Email is not valid');
    expect(result.body.errors[3]).toBe('Phone number must be a valid Austrian phone number');
  });

  it('should return errors when given email is duplicate', async () => {
    const result = await request(app).patch(`/referrals/${dummyReferralRecord1.id}`).send({
      givenName: 'newDummyGivenName',
      email: 'dummy1@email.com',
    });

    expect(result.status).toEqual(400);
    expect(result.body.errors.length).toBe(1);

    expect(result.body.errors[0]).toBe('Email already exists');
  });

  it('should create referral successfully', async () => {
    await request(app).post('/referrals').send({
      givenName: 'newDummyGivenName',
      surName: 'newDummySurname',
      email: 'new@email.com',
      phone: '0425555556',
    });

    const result = await request(app).get('/referrals');

    expect(result.status).toEqual(200);
    expect(Array.isArray(result.body)).toBe(true);
    expect(result.body.length).toBe(3);
  });

  it('should return errors when creating with insufficient fields', async () => {
    const result = await request(app).post('/referrals').send({
      givenName: null,
      surName: null,
      email: null,
      phone: null,
    });

    expect(result.status).toEqual(400);
    expect(result.body.errors.length).toBe(4);
    expect(result.body.errors[0]).toBe('Given Name should be 2 to 200 characters long');
    expect(result.body.errors[1]).toBe('Surname should be 2 to 200 characters long');
    expect(result.body.errors[2]).toBe('Email is not valid');
    expect(result.body.errors[3]).toBe('Phone number must be a valid Austrian phone number');
  });

  it('should return errors when data is invalid', async () => {
    const result = await request(app).post('/referrals').send({
      givenName: 'a',
      surName: 'b',
      email: 'invalidEmail',
      phone: '123456789',
    });

    expect(result.status).toEqual(400);
    expect(result.body.errors.length).toBe(4);
    expect(result.body.errors[0]).toBe('Given Name should be 2 to 200 characters long');
    expect(result.body.errors[1]).toBe('Surname should be 2 to 200 characters long');
    expect(result.body.errors[2]).toBe('Email is not valid');
    expect(result.body.errors[3]).toBe('Phone number must be a valid Austrian phone number');
  });

  it('should return errors when given email is duplicate', async () => {
    const result = await request(app).post('/referrals').send({
      givenName: 'newDummyGivenName',
      surName: 'newDummySurname',
      email: 'dummy1@email.com',
      phone: '0425555556',
    });

    expect(result.status).toEqual(400);
    expect(result.body.errors.length).toBe(1);
    expect(result.body.errors[0]).toBe('Email already exists');
  });

  it('should delete successfully', async () => {
    const result = await request(app).delete(`/referrals/${dummyReferralRecord2.id}`);

    expect(result.status).toEqual(200);
    expect(result.body.givenName).toBe('dummyGivenName2');
    expect(result.body.surName).toBe('dummySurname2');
  });

  it('should return record not found when update with non existing id', async () => {
    const result = await request(app).delete(`/referrals/${nonExistingId}`);

    expect(result.status).toEqual(404);
    expect(result.body.errors[0]).toBe('Record not found');
  });
});
