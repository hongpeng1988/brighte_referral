import { IsEmail, Length, IsPhoneNumber } from 'class-validator';
import { Unique } from '../validators/UniqueEmailValidator';

export class CreateReferralDTO {
  @Length(2, 200, { message: 'Given Name should be 2 to 200 characters long' })
  public givenName: string;

  @Length(2, 200, { message: 'Surname should be 2 to 200 characters long' })
  public surName: string;

  @IsEmail({}, { message: 'Email is not valid' })
  @Unique({
    message: 'Email already exists',
  })
  public email: string;

  @IsPhoneNumber('AU', { message: 'Phone number must be a valid Austrian phone number' })
  public phone: string;
}
