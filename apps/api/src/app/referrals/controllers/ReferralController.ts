import { Request, Response } from 'express';
import { plainToClass } from 'class-transformer';
import { validateOrReject } from 'class-validator';
import { Referral } from '@prisma/client';
import { ReferralService } from '../services/ReferralService';
import { CreateReferralDTO } from '../dtos/CreateReferralDTO';
import { UpdateReferralDTO } from '../dtos/UpateReferralDTO';

export default class ReferralController {
  private referralService: ReferralService;

  constructor() {
    this.referralService = new ReferralService();
  }

  async getAllReferrals(req: Request, res: Response) {
    const referrals: Referral[] = await this.referralService.getAllReferrals();
    res.json(referrals);
  }

  async getReferralById(req: Request, res: Response) {
    const { id }: { id?: number } = req.params;
    const referral: Referral = await this.referralService.getReferralById(id);

    if (referral == null) {
      return res.status(404).json({ errors: ['Record not found'] });
    }

    res.json(referral);
  }

  async updateById(req: Request, res: Response) {
    // validate inbound data
    try {
      const referralDTO = plainToClass(UpdateReferralDTO, req.body);
      await validateOrReject(referralDTO);
    } catch (errors) {
      const error_messages = errors.map((error) => {
        for (let property in error.constraints) {
          return error.constraints[property];
        }
      });
      return res.status(400).json({ errors: error_messages });
    }

    const { id }: { id?: number } = req.params;

    // check if id is valid
    const isIdValid = await this.referralService.isIdValid(id);
    if (!isIdValid) {
      return res.status(404).json({ errors: ['Record not found'] });
    }

    try {
      const referral: Referral = await this.referralService.updateReferralById(id, req.body);
      res.json(referral);
    } catch (err) {
      return res.status(500).json({ errors: ['Server error, please try again'] });
    }
  }

  async deleteById(req: Request, res: Response) {
    const { id }: { id?: number } = req.params;

    // check if id is valid
    const isIdValid = await this.referralService.isIdValid(id);
    if (!isIdValid) {
      return res.status(404).json({ errors: ['Record not found'] });
    }

    try {
      const referral: Referral = await this.referralService.deleteReferralById(id);
      res.json(referral);
    } catch (err) {
      return res.status(500).json({ errors: ['Server error, please try again'] });
    }
  }

  async createReferral(req: Request, res: Response) {
    // validate inbound data
    const referralDTO = plainToClass(CreateReferralDTO, req.body);
    try {
      await validateOrReject(referralDTO);
    } catch (errors) {
      const error_messages = errors.map((error) => {
        for (let property in error.constraints) {
          return error.constraints[property];
        }
      });
      return res.status(400).json({ errors: error_messages });
    }

    try {
      const referral: Referral = await this.referralService.createReferral(req.body);
      res.json(referral);
    } catch (err) {
      return res.status(500).json({ errors: ['Server error, please try again'] });
    }
  }
}
