import { Referral as ReferralModel } from '@prisma/client';
import prisma from '../../prisma';

export class ReferralService {
  async getReferralById(id: number): Promise<ReferralModel> {
    return await prisma.referral.findUnique({ where: { id: Number(id) } });
  }

  async isIdValid(id: number): Promise<boolean> {
    const referral = await prisma.referral.findFirst({
      where: { id: Number(id) },
    });
    return referral != null;
  }

  async emailAlreadyExists(email: string): Promise<boolean> {
    const referral = await prisma.referral.findFirst({
      where: { email },
    });
    return referral != null;
  }

  async getAllReferrals(): Promise<ReferralModel[]> {
    return await prisma.referral.findMany();
  }

  async deleteReferralById(id: number): Promise<ReferralModel> {
    return await prisma.referral.delete({ where: { id: Number(id) } });
  }

  async updateReferralById(id: number, data): Promise<ReferralModel> {
    return await prisma.referral.update({
      data,
      where: { id: Number(id) },
    });
  }

  async createReferral(data): Promise<ReferralModel> {
    return await prisma.referral.create({
      data,
    });
  }
}
